from functools import wraps
from mypyrt.bytecode import gen_bytecode


cdef PyObject *frame_func(PyFrameObject *frame_obj, int throwflag):
    frame = <object> frame_obj
    cdef str filepath = frame.f_code.co_filename
    cdef str funcname = frame.f_code.co_name
    cdef int lineno = frame.f_code.co_firstlineno

    if throwflag:
        return _PyEval_EvalFrameDefault(frame_obj, throwflag)

    if funcname not in frame.f_globals:
        return _PyEval_EvalFrameDefault(frame_obj, throwflag)

    func = frame.f_globals[funcname]

    if not getattr(func, "__annotations__", None):
        return _PyEval_EvalFrameDefault(frame_obj, throwflag)

    success, new_code = gen_bytecode(frame, func)
    Py_INCREF(new_code)
    frame_obj.f_code = <PyCodeObject *>new_code

    return _PyEval_EvalFrameDefault(frame_obj, throwflag)


cdef class Checker:
    cdef public bint enabled

    def __init__(self, bint enabled=0):
        # might need to be tls
        self.enabled = enabled

    cpdef enable(self):
        cdef PyThreadState *state = NULL
        if not self.enabled:
            state = PyThreadState_Get()
            state.interp.eval_frame = frame_func

    cpdef disable(self):
        cdef PyThreadState *state = NULL
        if self.enabled:
            state = PyThreadState_Get()
            state.interp.eval_frame = _PyEval_EvalFrameDefault

    cpdef __enter__(self):
        self.enable()
        return self

    cpdef __exit__(self, exc_type, exc_val, exc_tb):
        self.disable()
        return None

    def __call__(self, func):
        def wrapper():
            @wraps(func)
            def inner(*args, **kwargs):
                with self:
                    func(*args, **kwargs)
            return inner
        return wrapper
