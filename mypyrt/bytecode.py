import dis
from types import CodeType
import typing

checker_func = """
if not isinstance({val}, {type_}):
    raise TypeError("Bad type for {funcname}: Expected {type_} got {realtype}")
"""


def gen_bytecode(frame, func):
    filepath = frame.f_code.co_filename
    funcname = frame.f_code.co_name
    lineno = frame.f_code.co_firstlineno
    old_code = frame.f_code

    new_code = b''

    # we need to load isinstance, and the types into the symbol table
    # TODO: make this less specific to ints
    # TODO: also make this dynamic
    # load the actual objects into the consts tuple
    consts = old_code.co_consts + ("bad type", )
    #const_offset = len(old_code.co_consts)

    # load the names into the names table
    names = old_code.co_names + ('isinstance', 'int', 'TypeError')
    names_offset = len(old_code.co_names)

    for (name, type_) in func.__annotations__.items():
        # we're not gonna support return rn
        if name == 'return':
            continue

        try:
            val = frame.f_locals[name]
        except KeyError:
            try:
                val = frame.f_globals[name]
            except KeyError:
                print("failed on {name}".format(name=name))
                return False, old_code

        # need to add our literal to the const table
        consts += (val,)

        # need to detect if literal
        compiled = compile(
            checker_func.format(
                val=repr(val),
                type_=type_.__name__,
                funcname=funcname,
                realtype=type(val)), filepath, "exec")

        code = dis.Bytecode(compiled, first_line=lineno)
        code_array = bytearray(code.codeobj.co_code)
        for inst in code:
            if inst.opname == 'LOAD_CONST':
                if inst.arg == 1:
                    arg_offset = 1
                else:
                    arg_offset = consts.index(val)
                code_array[inst.offset + 1] = arg_offset
            if inst.opname == 'LOAD_NAME':
                arg_offset = inst.arg + names_offset
                code_array[inst.offset + 1] = arg_offset

        # cut off any return
        new_code += code_array[:-4]

    # rewrite offsets in the new_code
    print(new_code)

    ret = CodeType(
        old_code.co_argcount,
        old_code.co_kwonlyargcount,
        old_code.co_nlocals,
        old_code.co_stacksize,
        old_code.co_flags,
        new_code + old_code.co_code,
        consts,
        names,
        old_code.co_varnames,
        old_code.co_filename,
        old_code.co_name,
        old_code.co_firstlineno,
        old_code.co_lnotab,
        old_code.co_freevars,
        old_code.co_cellvars, )

    print(dis.dis(ret.co_code))
    print(ret.co_names)
    print(ret.co_consts)

    return True, ret
