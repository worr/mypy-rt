#!/usr/bin/env python
# vim:set fileencoding=utf8:
"""
test_mypy-rt
----------------------------------

Tests for `mypy-rt` module.
"""

import pytest

from mypyrt import Checker


def simple(x: int) -> int:
    print(type(x))
    return x


class Simple:
    def __init__(self, x: int) -> None:
        self.x = x

    def simple(self, x: int) -> int:
        return x

    @classmethod
    def simple_classmethod(cls, x: int) -> int:
        return x

    @staticmethod
    def simple_staticmethod(x: int) -> int:
        return x

    @property
    def simple_property(self) -> int:
        return self.x

    @simple_property.setter
    def simple_property(self, x: int) -> int:
        return self.x


def test_basic() -> None:
    Checker().enable()
    simple(5)

    with pytest.raises(TypeError):
        simple("foo")
    Checker().disable()


def test_inline() -> None:
    def inline(x: int) -> int:
        return x

    Checker().enable()
    inline(5)

    with pytest.raises(TypeError):
        inline("foo")

    Checker().disable()


def test_class() -> None:
    Checker().enable()
    s = Simple(5)

    with pytest.raises(TypeError):
        Simple("foo")

    s.simple(5)
    with pytest.raises(TypeError):
        s.simple("foo")

    with pytest.raises(TypeError):
        Simple.simple(pytest, 5)

    s.simple_property = 5
    with pytest.raises(TypeError):
        s.simple_property = "foo"

    Simple.simple_classmethod(5)
    with pytest.raises(TypeError):
        Simple.simple_classmethod(5)

    Simple.simple_staticmethod(5)
    with pytest.raises(TypeError):
        Simple.simple_staticmethod(5)
    Checker().disable()


def test_inline_class() -> None:
    class Inline:
        def inline(self, x: int) -> int:
            return x

    Checker().enable()
    i = Inline()

    i.inline(5)

    with pytest.raises(TypeError):
        i.inline("foo")
    Checker().disable()
