#!/usr/bin/env python
# vim:set fileencoding=utf8:

import os
from setuptools import setup, find_packages
from setuptools.extension import Extension

with open('README.mkd') as readme_file:
    readme = readme_file.read()

requirements = []

try:
    from Cython.Build import cythonize
    USE_CYTHON = True
    ext = ".pyx"
except:
    USE_CYTHON = False
    ext = ".c"

extensions = [Extension("mypyrt.mypyrt", [f"mypyrt/mypyrt{ext}"])]

if USE_CYTHON:
    ext_modules = cythonize(extensions)
else:
    ext_modules = extensions


setup(
    name='mypy-rt',
    version='0.1.0',
    description="Fast runtime mypy-based type-checking",
    long_description=readme,
    author="William Orr",
    author_email='will@worrbase.com',
    url='https://gitlab.com/worr/mypy-rt',
    packages=find_packages(),
    ext_modules=ext_modules,
    include_package_data=True,
    install_requires=requirements,
    license="MIT",
    zip_safe=False,
    keywords='mypy-rt',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
    ], )
